#!/usr/bin/env python

import sys
import rospy
import cv2
import numpy as np
import tf2_ros
from geometry_msgs.msg import TransformStamped

from sensor_msgs.msg import Image
from sensor_msgs.msg import CompressedImage
from sensor_msgs.msg import CameraInfo
from cv_bridge import CvBridge, CvBridgeError

class PoseEstimator():
	def __init__(self):
		# Set up the CV Bridge
		self.bridge = CvBridge()

		# Load in parameters from ROS
		self.param_use_compressed = rospy.get_param("~use_compressed", True)
		
		self.param_square_length = rospy.get_param("~square_length", 0.2)
		self.param_triangle_length = rospy.get_param("~triangle_length", 0.2)
		# Set additional camera parameters
		self.got_camera_info = False
		self.camera_matrix = None
		self.dist_coeffs = None

		# Set up the publishers, subscribers, and tf2
		self.sub_info = rospy.Subscriber("/raspicam_node/camera_info", CameraInfo, self.callback_info)

		self.sub_img = rospy.Subscriber('/raspicam_node/image/compressed', CompressedImage, self.callback_img)
		self.pub_img = rospy.Publisher("/image_overlay/compressed", CompressedImage, queue_size=1)
		



		self.tfbr = tf2_ros.TransformBroadcaster()

		

	def shutdown(self):
		# Unregister anything that needs it here
		self.sub_info.unregister()
		self.sub_img.unregister()

	# Collect in the camera characteristics
	def callback_info(self, msg_in):
		self.dist_coeffs = np.array([[msg_in.D[0], msg_in.D[1], msg_in.D[2], msg_in.D[3], msg_in.D[4]]], dtype="double")
		#rospy.loginfo("Distortion Coefficients")
		#rospy.loginfo(self.dist_coeffs)
		self.camera_matrix = np.array([(msg_in.P[0], msg_in.P[1], msg_in.P[2]), 
                 (msg_in.P[4], msg_in.P[5], msg_in.P[6]), 
                 (msg_in.P[8], msg_in.P[9], msg_in.P[10])], dtype="double")
		#rospy.loginfo("Camera Matrix")
		#rospy.loginfo(self.camera_matrix)

		if not self.got_camera_info:
			rospy.loginfo("Got camera info")
			self.got_camera_info = True

	def callback_img(self, msg_in):
		# Don't bother to process image if we don't have the camera calibration
		if self.got_camera_info:
			#Convert ROS image to CV image
			cv_image = None

			try:
				if self.param_use_compressed:
					cv_image = self.bridge.compressed_imgmsg_to_cv2( msg_in, "bgr8" )
				else:
					cv_image = self.bridge.imgmsg_to_cv2( msg_in, "bgr8" )
			except CvBridgeError as e:
				rospy.loginfo(e)
				return
		
			cv_image = self.bridge.compressed_imgmsg_to_cv2(msg_in, "bgr8")
			# Perform a colour mask for detection
			#cv2.namedWindow("Trackbar")
			#cv2.createTrackbar("LowH","Trackbar", 0, 180, nothing)
			#cv2.createTrackbar("LowS","Trackbar", 82, 255, nothing)
			#cv2.createTrackbar("LowV","Trackbar", 53, 255, nothing)
			#cv2.createTrackbar("HighH","Trackbar", 114, 180, nothing)
			#cv2.createTrackbar("HighS","Trackbar", 255, 255, nothing)
			#cv2.createTrackbar("HighV","Trackbar", 255, 255, nothing)

			#LowH = cv2.getTrackbarPos("LowH", "Trackbar")
			LowH = 0
			#LowS = cv2.getTrackbarPos("LowS", "Trackbar")
			LowS = 82
			#LowV = cv2.getTrackbarPos("LowV", "Trackbar")
			LowV = 53
			#HighH = cv2.getTrackbarPos("HighH", "Trackbar")
			HighH = 114
			#HighS = cv2.getTrackbarPos("HighS", "Trackbar")
			HighS = 255
			#HighV = cv2.getTrackbarPos("HighV", "Trackbar")
			HighV = 255

			HSV = cv2.cvtColor(cv_image,cv2.COLOR_BGR2HSV)
			GRAY = cv2.cvtColor(cv_image,cv2.COLOR_BGR2GRAY)
			# every color except white
			low_nwhite = np.array([LowH, LowS, LowV]) # 0, 82, 53
			high_nwhite = np.array([HighH, HighS, HighV]) #114, 255, 255
			nwhite_mask = cv2.inRange(HSV, low_nwhite, high_nwhite)
			nwhite = cv2.bitwise_and(cv_image,cv_image,mask=nwhite_mask) #makes detection of every color except white instead of white
			#######################
			_, contours, _ = cv2.findContours(nwhite_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

			font = cv2.FONT_HERSHEY_COMPLEX
			# Generate the model for the pose solver
			# For this example, draw a square around where the circle should be
			# There are 5 points, one in the center, and one in each corner
			l = self.param_square_length
			self.model_square = np.array([(0.0, 0.0, 0.0), 
							(l/2.0, l/2.0, 0.0), 
							(l/2.0, -l/2.0, 0.0),
							(-l/2.0, l/2.0, 0.0),
							(-l/2.0, -l/2.0, 0.0)])

			b = self.param_triangle_length
			self.model_triangle = np.array([(0.0, 0.0, 0.0), 
							(b/2.0, b/2.0, 0.0), 
							(b/2.0, -b/2.0, 0.0),
							(-b/2.0, b/2.0, 0.0),
							(-b/2.0, -b/2.0, 0.0)])

			for cnt in contours:
				area = cv2.contourArea(cnt)
				approx = cv2.approxPolyDP(cnt, 0.04*cv2.arcLength(cnt, True), True) 
				
				ret,thresh = cv2.threshold(GRAY,127,255,0)
				M = cv2.moments(thresh)
				#px = int(M["m10"] / M["m00"])
				#py = int(M["m01"] / M["m00"])
				px = 0
				py = 0						
				
				if area > 400:
					cv2.drawContours(cv_image, [approx], 0, (150), 2) #second number is colour of contour, third is line weight
					
					x = approx.ravel()[0]
					y = approx.ravel()[1]
					if len(approx) == 3:
						pix_len = cv2.arcLength(approx, True)/3
						rospy.loginfo("Triangle method started")
						#print(approx) # This gives pixel location of each vertice
						cv2.putText(cv_image, "Triangle", (x, y), font, 1, (255))
						rospy.loginfo("Triangle text placed")
						cv2.circle(cv_image, (px, py), 3, (255,255,255), -1)
						rospy.loginfo("Circle placed at centre of triangle")
						self.model_image_triangle = np.array([	(px, py), 
											(px+(pix_len/2), py+(pix_len/2)), 
											(px+(pix_len/2), py-(pix_len/2)), 
											(px-(pix_len/2), py+(pix_len/2)), 
											(px-(pix_len/2), py-(pix_len/2))])
						rospy.loginfo("Triangle model created")
						(success_t, rvec_t, tvec_t) = cv2.solvePnP(self.model_triangle, self.model_image_triangle, self.camera_matrix, self.dist_coeffs)
						rospy.loginfo("Triangle solvePNP method completed")
						# If a triangle result was found above 1 metre, send to TF2
						if success_t:
							time_found = rospy.Time.now()
							msg_out = TransformStamped()
							msg_out.header = msg_in.header
							msg_out.child_frame_id = "triangle"
							msg_out.transform.translation.x = tvec_t[0]
							msg_out.transform.translation.y = tvec_t[1]
							msg_out.transform.translation.z = tvec_t[2]
							msg_out.transform.rotation.w = 1.0	# Could use rvec, but need to convert from DCM to quaternion first
							msg_out.transform.rotation.x = 0.0
							msg_out.transform.rotation.y = 0.0
							msg_out.transform.rotation.z = 0.0

							self.tfbr.sendTransform(msg_out)
							#self.pub_found.publish(time_found)


					elif len(approx) == 4:
						pix_len_s = cv2.arcLength(approx, True)/4
						rospy.loginfo("Square method started")
						#print(approx) # This gives pixel location of each vertice
						cv2.putText(cv_image, "Square", (x, y), font, 1, (255))
						rospy.loginfo("Square text placed")
						cv2.circle(cv_image, (px, py), 3, (255,255,255), -1)
						rospy.loginfo("Center circle placed on square")
						self.model_image_square = np.array([	(px, py), 
											(px+pix_len_s/2, py+pix_len_s/2), 
											(px+pix_len_s/2, py-pix_len_s/2),
											(px-pix_len_s/2, py+pix_len_s/2),
											(px-pix_len_s/2, py-pix_len_s/2)])
						rospy.loginfo("Square model created")
						# Do the SolvePnP method for square
						(success_s, rvec_s, tvec_s) = cv2.solvePnP(self.model_square, self.model_image_square, self.camera_matrix, self.dist_coeffs)
						rospy.loginfo("Square SolvePNP done")
						# If a square result was found above 1 metre, send to TF2
						if success_s:
							time_found = rospy.Time.now()
							msg_out = TransformStamped()
							msg_out.header = msg_in.header
							msg_out.child_frame_id = "square"
							msg_out.transform.translation.x = tvec_s[0]
							msg_out.transform.translation.y = tvec_s[1]
							msg_out.transform.translation.z = tvec_s[2]
							msg_out.transform.rotation.w = 1.0	# Could use rvec, but need to convert from DCM to quaternion first
							msg_out.transform.rotation.x = 0.0
							msg_out.transform.rotation.y = 0.0
							msg_out.transform.rotation.z = 0.0

							self.tfbr.sendTransform(msg_out)
							#self.pub_found.publish(time_found)
							# Do the SolvePnP method for triangle



			
			# Convert CV image to ROS image and publish
			try:
	
				self.pub_img.publish(self.bridge.cv2_to_compressed_imgmsg(cv_image))
			except CvBridgeError as e:
				rospy.logerr(e)
		

if __name__ == '__main__':
	# Initialize
	rospy.init_node('egh450_image_processor', anonymous=True)

	ip = PoseEstimator()
	rospy.loginfo("[IMG] Processing images...")

	# Loop here until quit
	rospy.spin()


	