#!/usr/bin/env python

import sys
import rospy
import cv2
import numpy as np
import tf2_ros
import math
from geometry_msgs.msg import TransformStamped

from sensor_msgs.msg import Image
from sensor_msgs.msg import CompressedImage
from sensor_msgs.msg import CameraInfo
from cv_bridge import CvBridge, CvBridgeError

class PoseEstimator():
	def __init__(self):
		# Set up the CV Bridge
		self.bridge = CvBridge()

		# Load in parameters from ROS
		self.param_use_compressed = rospy.get_param("~use_compressed", True)
		
		self.param_square_length = rospy.get_param("~square_length", 0.2)
		self.param_triangle_length = rospy.get_param("~triangle_length", 0.2)
		# Set additional camera parameters
		self.got_camera_info = False
		self.camera_matrix = None
		self.dist_coeffs = None

		# Set up the publishers, subscribers, and tf2
		self.sub_info = rospy.Subscriber("/raspicam_node/camera_info", CameraInfo, self.callback_info)

		self.sub_img = rospy.Subscriber('/raspicam_node/image/compressed', CompressedImage, self.callback_img)
		self.pub_img = rospy.Publisher("/image_overlay/compressed", CompressedImage, queue_size=1)
		



		self.tfbr = tf2_ros.TransformBroadcaster()

		rospy.loginfo("Publishers and subscribers setup")

	def shutdown(self):
		# Unregister anything that needs it here
		self.sub_info.unregister()
		self.sub_img.unregister()

	def unit_vector(self, v):
		return v/np.linalg.norm(v)

	def get_angle(self, p1, p2, p3):
		v1 = np.array([p1[0] - p2[0], p1[1] - p2[1]]) #creating vectors for dot product
		v2 = np.array([p1[0] - p3[0], p1[1] - p3[1]])
		v1_unit = self.unit_vector(v1)
		v2_unit = self.unit_vector(v2)
		radians = np.arccos(np.clip(np.dot(v1_unit,v2_unit), -1, 1))
		return math.degrees(radians)

	# Collect in the camera characteristics
	def callback_info(self, msg_in):
		self.dist_coeffs = np.array([[msg_in.D[0], msg_in.D[1], msg_in.D[2], msg_in.D[3], msg_in.D[4]]], dtype="double")
		rospy.loginfo("Distortion Coefficients")
		rospy.loginfo(self.dist_coeffs)
		self.camera_matrix = np.array([(msg_in.P[0], msg_in.P[1], msg_in.P[2]), 
                 (msg_in.P[4], msg_in.P[5], msg_in.P[6]), 
                 (msg_in.P[8], msg_in.P[9], msg_in.P[10])], dtype="double")
		rospy.loginfo("Camera Matrix")
		rospy.loginfo(self.camera_matrix)

		if not self.got_camera_info:
			rospy.loginfo("Did not get camera info")
			self.got_camera_info = True

	def callback_img(self, msg_in):
		# Don't bother to process image if we don't have the camera calibration
		#rospy.loginfo("Callback initiated")
		if self.got_camera_info:
			rospy.loginfo("Got camera info")
			#Convert ROS image to CV image
			cv_image = None

			try:
				if self.param_use_compressed:
					cv_image = self.bridge.compressed_imgmsg_to_cv2( msg_in, "bgr8" )
					rospy.loginfo("Converting Compressed Imgmsg to Cv2")
				else:
					cv_image = self.bridge.imgmsg_to_cv2( msg_in, "bgr8" )
					rospy.loginfo("Converting Raw Imgmsg to Cv2")
			except CvBridgeError as e:
				rospy.loginfo(e)
				return
		
			cv_image = self.bridge.compressed_imgmsg_to_cv2(msg_in, "bgr8")
		
			HSV = cv2.cvtColor(cv_image,cv2.COLOR_BGR2HSV)
			#rospy.loginfo("Color to HSV space completed")
			GRAY = cv2.cvtColor(cv_image,cv2.COLOR_BGR2GRAY)


			#red color

			low_red = np.array([2,70,20])
			high_red = np.array([25,255,255])

			red_mask = cv2.inRange(HSV, low_red, high_red)
			red = cv2.bitwise_and(cv_image,cv_image,mask=red_mask) #makes detection red instead of white

			# blue color
			low_blue = np.array([94,70,2])
			high_blue = np.array([126,255,255])

			blue_mask = cv2.inRange(HSV, low_blue, high_blue)
			blue = cv2.bitwise_and(cv_image,cv_image,mask=blue_mask) #makes detection blue instead of white

			
			#######################
			_, con_blue, _ = cv2.findContours(blue_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
			#rospy.loginfo("Blue contours found")
			_, con_red, _ = cv2.findContours(red_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
			#rospy.loginfo("Orange contours found")

			font = cv2.FONT_HERSHEY_COMPLEX

			b = self.param_triangle_length
			self.model_triangle = np.array([(0.0, 0.0, 0.0), 
							(b/2.0, b/2.0, 0.0), 
							(b/2.0, -b/2.0, 0.0),
							(-b/2.0, b/2.0, 0.0),
							(-b/2.0, -b/2.0, 0.0)])
			l = self.param_square_length
			self.model_square = np.array([(0.0, 0.0, 0.0), 
							(l/2.0, l/2.0, 0.0), 
							(l/2.0, -l/2.0, 0.0),
							(-l/2.0, l/2.0, 0.0),
							(-l/2.0, -l/2.0, 0.0)])

			for cnt in con_blue:
				area = cv2.contourArea(cnt)
				approx = cv2.approxPolyDP(cnt, 0.04*cv2.arcLength(cnt, True), True) 
				
				ret,thresh = cv2.threshold(GRAY,127,255,0)
				M = cv2.moments(thresh)
				
				if M["m00"] != 0:
					px = int(M["m10"] / M["m00"])
					py = int(M["m01"] / M["m00"])
				
				else: 
					px = 0
					py = 0						
				
				if 1000 < area and len(approx) == 3:
		
					p1t = approx[0][0]
					#rospy.loginfo("Tri P1")
					#rospy.loginfo(p1t)
					p2t = approx[1][0]
					#rospy.loginfo("Tri P2")
					#rospy.loginfo(p2t)
					p3t = approx[-1][0]
					degrees = self.get_angle(p1t,p2t,p3t)
					rospy.loginfo("Triangle angle")
					rospy.loginfo(degrees)
					
					if 55 < degrees < 65:
						cv2.drawContours(cv_image, [approx], 0, (150), 2) #second number is colour of contour, third is line weight
				
						x = approx.ravel()[0]
						y = approx.ravel()[1]						

						pix_len = cv2.arcLength(approx, True)/3
						#rospy.loginfo("Triangle method started")
						#print(approx) # This gives pixel location of each vertice
						cv2.putText(cv_image, "Triangle", (x, y), font, 1, (255))
						#rospy.loginfo("Triangle text placed")
						cv2.circle(cv_image, (px, py), 3, (0,255,255), -1)
						#rospy.loginfo("Circle placed at centre of triangle")
						self.model_image_triangle = np.array([	(px, py), 
											(px+(pix_len/2), py+(pix_len/2)), 
											(px+(pix_len/2), py-(pix_len/2)), 
											(px-(pix_len/2), py+(pix_len/2)), 
											(px-(pix_len/2), py-(pix_len/2))])
						rospy.loginfo("Triangle model created")
						
						(success_t, rvec_t, tvec_t) = cv2.solvePnP(self.model_triangle, self.model_image_triangle, self.camera_matrix, self.dist_coeffs)
						rospy.loginfo("Triangle solvePNP method completed")
						# If a triangle result was found above 1 metre, send to TF2
						if success_t and tvec_s[2] > 1.0:
							time_found = rospy.Time.now()
							msg_out = TransformStamped()
							msg_out.header = msg_in.header
							msg_out.child_frame_id = "triangle"
							msg_out.transform.translation.x = tvec_t[0]
							msg_out.transform.translation.y = tvec_t[1]
							msg_out.transform.translation.z = tvec_t[2]
							msg_out.transform.rotation.w = 1.0	# Could use rvec, but need to convert from DCM to quaternion first
							msg_out.transform.rotation.x = 0.0
							msg_out.transform.rotation.y = 0.0
							msg_out.transform.rotation.z = 0.0

							self.tfbr.sendTransform(msg_out)
							self.pub_found.publish(time_found)


			for cnt in con_red:
				area = cv2.contourArea(cnt)
				approx = cv2.approxPolyDP(cnt, 0.04*cv2.arcLength(cnt, True), True) 
				
				ret,thresh = cv2.threshold(GRAY,127,255,0)
				M = cv2.moments(thresh)
				
				if M["m00"] != 0:
					px = int(M["m10"] / M["m00"])
					py = int(M["m01"] / M["m00"])
					
				else: 
					px = 0
					py = 0						
				
				if 600 < area < 1400 and len(approx) == 4:							
						
					p1s = approx[0][0]
					#rospy.loginfo("Squ P1")
					#rospy.loginfo(p1s)
					p2s = approx[1][0]
					#rospy.loginfo("Squ P2")
					#rospy.loginfo(p2s)
					p3s = approx[-1][0]
					degrees = self.get_angle(p1s,p2s,p3s)
					rospy.loginfo("Square angle")
					rospy.loginfo(degrees)

					if 85 < degrees < 95:
						cv2.drawContours(cv_image, [approx], 0, (150), 2) #second number is colour of contour, third is line weight
				
						x = approx.ravel()[0]
						y = approx.ravel()[1]		
						pix_len_s = cv2.arcLength(approx, True)/4
						#rospy.loginfo("Square method started")
						#print(approx) # This gives pixel location of each vertice
						cv2.putText(cv_image, "Square", (x, y), font, 1, (255))
						#rospy.loginfo("Square text placed")
						cv2.circle(cv_image, (px, py), 3, (255,255,255), -1)
						#rospy.loginfo("Center circle placed on square")
						self.model_image_square = np.array([	(px, py), 
											(px+pix_len_s/2, py+pix_len_s/2), 
											(px+pix_len_s/2, py-pix_len_s/2),
											(px-pix_len_s/2, py+pix_len_s/2),
											(px-pix_len_s/2, py-pix_len_s/2)])
						rospy.loginfo("Square model created")

						# Do the SolvePnP method for square
						(success_s, rvec_s, tvec_s) = cv2.solvePnP(self.model_square, self.model_image_square, self.camera_matrix, self.dist_coeffs)
						rospy.loginfo("Square SolvePNP done")
						# If a square result was found above 1 metre, send to TF2
						if success_s and tvec_s[2] > 1.0:
							time_found = rospy.Time.now()
							msg_out = TransformStamped()
							msg_out.header = msg_in.header
							msg_out.child_frame_id = "square"
							msg_out.transform.translation.x = tvec_s[0]
							msg_out.transform.translation.y = tvec_s[1]
							msg_out.transform.translation.z = tvec_s[2]
							msg_out.transform.rotation.w = 1.0	# Could use rvec, but need to convert from DCM to quaternion first
							msg_out.transform.rotation.x = 0.0
							msg_out.transform.rotation.y = 0.0
							msg_out.transform.rotation.z = 0.0

							self.tfbr.sendTransform(msg_out)
							self.pub_found.publish(time_found)
							


			
			# Convert CV image to ROS image and publish
			try:
	
				self.pub_img.publish(self.bridge.cv2_to_compressed_imgmsg(cv_image))
			except CvBridgeError as e:
				rospy.logerr(e)
		

if __name__ == '__main__':
	# Initialize
	rospy.init_node('egh450_image_processor', anonymous=True)

	ip = PoseEstimator()
	rospy.loginfo("[IMG] Processing images...")

	# Loop here until quit
	rospy.spin()


	
